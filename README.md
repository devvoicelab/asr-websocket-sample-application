### Application "websocket-api" reads audio files, sends them to ASR using WebSocket and receives recognition.

&nbsp;

#### 1. Simple usage is:

```sh
    ./websocket-api-16000-en_US -f PATH_TO_FILE
```

&nbsp;
