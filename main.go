package main

import (
	"encoding/json"
	"flag"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/gorilla/websocket"
)

var file = flag.String("f", "", "file to send")
var authJSON = flag.Bool("json", false, "send auth data in json")

type VLHeaders struct {
	XVoicelabPid      string `json:"X-Voicelab-Pid"`
	XVoicelabPassword string `json:"X-Voicelab-Password"`
	Authorization     string `json:"Authorization"`
	XVoicelabConfName string `json:"X-Voicelab-Conf-Name"`
	XVoicelabOptions  string `json:"X-Voicelab-Options"`
	XVoicelabCodec    string `json:"X-Voicelab-Codec"`
	ContentType       string `json:"Content-Type"`
}

type Update struct {
	Status   string   `json:"status"`
	Shift    int      `json:"shift"`
	Words    []string `json:"words"`
	Start    []int64  `json:"start,omitempty"`
	End      []int64  `json:"end,omitempty"`
	Duration *int64   `json:"duration,omitempty"`
}

func main() {
	flag.Parse()
	log.SetFlags(0)

	addr := "demo.voicelab.ai"
	if *file == "" {
		log.Fatalf("Error, No file to open!")
	}

	u := url.URL{Scheme: "wss", Host: addr, Path: "/classify/asr"}
	if *authJSON {
		u = url.URL{Scheme: "wss", Host: addr, Path: "/classify/asr", RawQuery: "auth=json"}
	}
	log.Printf("connecting to %s", u.String())

	headers := make(http.Header)
	headers.Add("Content-Type", "audio/l16;rate=8000")
	headers.Add("X-Voicelab-Pid", "109")
	headers.Add("X-Voicelab-Password", "fbcd6fbb37a10a6d44467918a67d6c54")
	headers.Add("X-Voicelab-Conf-Name", "8000_en_US")

	if *authJSON {
		headers = nil
	}
	conn, _, err := websocket.DefaultDialer.Dial(u.String(), headers)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer conn.Close()

	if *authJSON {
		var authData *VLHeaders
		authData = &VLHeaders{
			XVoicelabPid:      "109",
			XVoicelabPassword: "fbcd6fbb37a10a6d44467918a67d6c54",
			XVoicelabConfName: "8000_en_US",
			XVoicelabOptions:  "timestamps",
			ContentType:       "audio/l16;rate=8000"}

		err = conn.WriteJSON(authData)
		if err != nil {
			log.Fatal("on open:", err)
		}
	}

	go func() {
		f, err := os.Open(*file)
		f.Seek(44, 0)
		defer f.Close()
		var fourZeros = []byte{0, 0, 0, 0}
		buff := make([]byte, 4096)
		for {
			n, err1 := io.ReadFull(f, buff)
			if err1 == io.EOF {
				err = conn.WriteMessage(websocket.BinaryMessage, fourZeros)
				break
			}
			if err1 != nil && err1 != io.ErrUnexpectedEOF {
				log.Println("Error read data from file: ", err)
				break
			}
			err = conn.WriteMessage(websocket.BinaryMessage, buff[:n])
			if err != nil {
				log.Println("Error sending data to moonlight: ", err)
				break
			}
			if err1 == io.ErrUnexpectedEOF {
				log.Printf("Finish sending audio!")
				err = conn.WriteMessage(websocket.BinaryMessage, fourZeros)
				break
			}
		}
	}()

	var recognition []string
	for {
		messageType, message, err := conn.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			return
		}
		if messageType == websocket.TextMessage {
			log.Printf("recv: %s", message)
			var msg *Update
			msg = &Update{}
			err = json.Unmarshal([]byte(message), msg)
			if err != nil {
				log.Fatal(err)
			}
			if msg != nil {
				lengthRec := len(recognition) + msg.Shift - len(msg.Words)
				recognition = recognition[:lengthRec]
				for _, p := range msg.Words {
					recognition = append(recognition, p)
				}
			}
		}
		if messageType == websocket.BinaryMessage {
			log.Println("end!")
			break
		}
	}
	log.Printf("Recognition: %s\r\n", strings.Join(recognition, " "))
}
